# Fixnum class methods

  # number increment method
    # succ or next

    a_number = 12345
    puts a_number.class
    puts "a_number = #{a_number} \nsucc"
    a_number = a_number.succ
    puts "a_number = " + a_number.to_s

  # is number zero?
    # zero?

    puts "\na_number zero?"
    puts a_number.zero?
    puts "a_number = 0"
    a_number = 0
    puts a_number.zero?

  # Absolute value
    # abs

    puts "\nABS"
    a_number = -12345
    puts "a_number = " + a_number.to_s
    puts "abs a_number = #{a_number.abs}"

# Float class methods

  # round-off down to integer part
    # floor

    a_number = 12345.6789
    puts  a_number.class
    puts "\nfloor"
    puts "a_number = #{a_number}"
    puts "floor a_number = #{a_number.floor}"

  # result of division as an array of integer part and modulo
    # divmod

    puts "\ndivmod"
    div_array = a_number.divmod(1)
    puts "divmod(1): integer part = #{div_array[0]}, modulo = #{div_array[1]}"
    puts div_array.to_s
    div_array = a_number.divmod(2)
    puts "divmod(2): integer part = #{div_array[0]}, modulo = #{div_array[1]}"
    puts div_array.to_s

# String class methods

  # Downcase the contents of str
    # downcase

    a_string = "TeSt StRiNg!"
    puts  "\n" + a_string.class.to_s
    puts a_string
    puts "downcase"
    puts a_string.downcase!

    # Return next ASCII character
      # next! or succ!

    puts "\nnext!"
    a_string = "B"
    puts a_string
    a_string.next!
    puts "a.next! = #{a_string}"

    # Remove leading and trailing whitespace from str
      # strip!

    puts "\nstrip!"
    a_string = "     Test!     "
    puts "a_string =\'#{a_string}\'"
    a_string.strip!
    puts "a_string.strip! =\'#{a_string}\'"

# Array class methods

    # Delete all items from array that are equal to
      # delete

    arry = [ "a", "b", "c", "c", "c", "d" ]
    puts "\n" + arry.class.to_s + "\n" + arry.to_s + "\ndelete \'c\'"
    arry.delete("c")
    puts arry.to_s

    # Shuffle array
      # shuffle

    arry = [ "a", "b", "c", "d", "e", "f" ]
    puts "\n#{arry.to_s}\nshuffle!"
    arry.shuffle!
    puts arry.to_s

    # Convert to string with concat
      # join or *

    arry = [ "a", "b", "c", "d", "e", "f" ]

# Hash class methods

    # Key exists?
      # key?

    c_hash = {"one" => 1, "two" => 2, "three" => 3, "four" => 4, "five" => 5}
    puts "\n#{c_hash.class.to_s}\n#{c_hash.to_s}"
    puts "Is there three key? => #{c_hash.key?("three")}"
    puts "Is there six key? => #{c_hash.key?("six")}"

    # Merge
      # merge

    c_hash = {"one" => 1, "two" => 2, "three" => 3, "four" => 4, "five" => 5}
    d_hash = {"six" => 6, "seven" => 7}
    puts "\nmerge"
    puts "#{c_hash.to_s}\n#{d_hash.to_s}"
    puts e_hash = c_hash.merge(d_hash)

# Range class methods

    # Max range element
      # max

    r_range = (-5..25)
    puts "\n#{r_range.class.to_s}"
    puts "Max range #{r_range.to_s} element = #{r_range.max.to_s}"
    r_range = (-5...25)
    puts "Max range #{r_range.to_s} element = #{r_range.max.to_s}"