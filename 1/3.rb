# improve code:

results = [10, 2, 5, 12, 11, 11, 4]
for sum in results
  puts sum
end

puts "--------------or-----------------\n"

game = 1
for sum in results
  puts "Game: #{game.to_s} / Score: #{sum.to_s}"
  game += 1
end

puts "--------------or-----------------\n"

game = 0
for sum in results
  puts "Game: #{game += 1} / Score: #{sum.to_s}"
end

puts "--------------or-----------------\n"

(1 .. results.length).each do |game|
  puts "Game: #{game.to_s} / Score: #{results[game - 1].to_s}"
end